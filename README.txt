
FANCY THEME README
Sandeep Sangamreddi


RELEASE NOTES
=============

This is my first Drupal phptempalte based theme. I've made few enhancements for the 
benefit of fancy look.Graphical tabs colored blocks, and sIFR. This theme represents 
a major overhaul of my approach to 
theming and sets the foundation for my future theming efforts. 
Download sIFR http://www.mikeindustries.com/sifr/ 
If you need to change the sIFR titles with your own fonts, download the sIFR package and 
create your won fonts by creating your flash file.
The self explained css makes you to change easily. For blocks customization go to blocks.css 
The latest akbar font can be downloaded from http://www.wobblymusic.com/groening/akbar.html

Features
--------

*Graphical Tabs for primary links
*Each Tab has different color Supports upto 10 Different Graphical tabs
*Different colors for blocks, all the core blocks have been customized with different look.
*Customized blocks for contributed modules Article,Event,Gallery,Simplenews,Image,Video, i18n
*sIFR for Site Name & Node titles - Fancy Node titles


ENABLE/DISABLE sIFR for node titles
-----------------------------------
By default the Fancy theme comes with sIFR enable. 

Disable sIFR 

Open the node.tpl.php and on line No:5 remove "sifr-" from the node h2 class and save.
Ex:
h2 classs="sifr-node-title"   to      h2 class="node-title"

Please don't remove the sIFR folder, the SiteName still uses it.


TO DO
=====


Contact Author
===============
http://www.gleez.com/contact