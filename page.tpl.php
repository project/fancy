<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language; ?>" xml:lang="<?php print $language; ?>">
<head>
<title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
	<script src="<?php print $base_path ?><?php print $directory ?>/sifr/sifr.js" type="text/javascript"></script>
	<script src="<?php print $base_path ?><?php print $directory ?>/sifr/sifr-addons.js" type="text/javascript"></script>
	<script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>
<body<?php print $onload_attributes ?>>
<table border="0" cellpadding="0" cellspacing="0" id="header">
  <tr>
    <td rowspan="2" id="logo"> <?php if ($logo) { ?>      <a href="<?php print $base_path ?>" title="Home"><img src="<?php print $logo ?>" alt="Home" /></a>      <?php } ?></td>
    <td colspan="2" id="prime-top"> <div id="primary-tabs">
	
 <?php print theme('navlinks', $primary_links); ?>

  </div></td>
  </tr>
  <tr>
  <td id="fname">
      <?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $base_path ?>" title="Home"><?php print $site_name ?></a></h1><?php } ?>
      <?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>    </td>
    
    <td id="menu"><?php print $search_box ?><?php if (isset($secondary_links)) { ?><div id="secondary"><?php print theme('links', $secondary_links) ?></div><?php } ?>
	</td>
  </tr>
  <?php if ($header) { ?>
     <tr>
    <td colspan="3"><div><?php print $header ?></div></td>
  </tr>
  <?php } ?>
</table>

<table border="0" cellpadding="0" cellspacing="0" id="content">
  <tr>
    <?php if ($sidebar_left) { ?><td id="sidebar-left">
      <?php print $sidebar_left ?>
    </td><?php } ?>
    <td valign="top">
      <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
      <div id="main">
        <?php print $breadcrumb ?>
		 <?php if ($node == 0): ?>
        <h1 class="title"><?php print $title ?></h1>   <?php endif; ?>
        <div class="tabs"><?php print $tabs ?></div>
        <?php print $help ?>
        <?php print $messages ?>

        <?php print $content; ?>
      </div>
    </td>
    <?php if ($sidebar_right) { ?><td id="sidebar-right">
      <?php print $sidebar_right ?>
    </td><?php } ?>
  </tr>
</table>

<table id="footer" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th id="copyright" scope="col">Copyright &copy; <?php print $site_name ?> <br />
      Powered by <a href="http://www.drupal.org" target="_blank">Drupal</a> <br />
      Designed by <a href="http://www.gleez.com" target="_blank">Gleez</a> <br />
    </th>
    <th id="footer-msg" scope="col"> 	 <?php if (isset($primary_links)) { ?><div id="primary" align="center"><?php print theme('links', $primary_links) ?></div>		      <?php } ?><br />
	<?php print $footer_message ?></th>
  </tr>
</table>

<?php print $closure ?>
<script type="text/javascript">
//<![CDATA[

if(typeof sIFR == "function"){

// Site Name
sIFR.replaceElement(named({sSelector:"h1.site-name", sFlashSrc:"<?php print $base_path ?><?php print $directory ?>/sifr/vandenkeere.swf", sColor:"#ce5003", sLinkColor:"#2a2ad8", sWmode:"transparent", sHoverColor:"#ffffff", nPaddingTop:0, nPaddingBottom:0, sFlashVars:"textalign=left&offsetTop=0"}));

// Node Title
sIFR.replaceElement(named({sSelector:"h2.sifr-node-title", sFlashSrc:"<?php print $base_path ?><?php print $directory ?>/sifr/akbar.swf", sColor:"#FF4500", sLinkColor:"#FF4500", sBgColor:"#ffffff", sHoverColor:"#F88017", nPaddingTop:0, nPaddingBottom:0, sFlashVars:"textalign=left&offsetTop=0"}));

};

//]]>
</script>
</body>
</html>