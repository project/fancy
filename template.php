<?php

function fancy_navlinks($links) {
  $output = '';

  if (count($links) > 0) {
    $output = '<ul>';

       $i = 1;

    foreach ($links as $key => $link) {
      $class = '';

// Automatically add a current LI class ='current' to active link
      if ( stristr($key, 'active') ){
        $class = ' class="current"';
       }


      $output .= '<li id="primary-link-'.$i.'"' .  $class .' >';

      // Is the title HTML?
      $html = isset($link['html']) && $link['html'];

      // Initialize fragment and query variables.
      $link['query'] = isset($link['query']) ? $link['query'] : NULL;
      $link['fragment'] = isset($link['fragment']) ? $link['fragment'] : NULL;

      if (isset($link['href'])) {
        $output .= l($link['title'], $link['href'], $link['attributes'], $link['query'], $link['fragment'], FALSE, $html);
      }
      else if ($link['title']) {
        //Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (!$html) {
          $link['title'] = check_plain($link['title']);
        }
        $output .= '<span'. drupal_attributes($link['attributes']) .'>'. $link['title'] .'</span>';
      }

      $i++;
      $output .= "</li>\n";
    }

    $output .= '</ul>';
  }

  return $output;
}

?>